//
//  ViewController.h
//  TestActivityIndicator
//
//  Created by Divya Munni on 6/30/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)didStartAnimation:(id)sender;

- (IBAction)didStopAnimation:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *sampleActivityIndicator;

@end

