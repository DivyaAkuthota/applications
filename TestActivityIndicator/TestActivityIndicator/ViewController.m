//
//  ViewController.m
//  TestActivityIndicator
//
//  Created by Divya Munni on 6/30/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sampleActivityIndicator.hidden = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didStartAnimation:(id)sender {
    [self.sampleActivityIndicator startAnimating];
}

- (IBAction)didStopAnimation:(id)sender {
    [self.sampleActivityIndicator stopAnimating];
}
@end
