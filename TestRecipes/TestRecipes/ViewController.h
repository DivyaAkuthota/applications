//
//  ViewController.h
//  TestRecipes
//
//  Created by Divya Munni on 6/19/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@end

