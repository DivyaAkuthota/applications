//
//  ViewController.h
//  RecipeBook
//
//  Created by Divya Munni on 2/27/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *RecipeTableView;

@end

