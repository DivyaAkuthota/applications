//
//  ViewController.m
//  CoreDataSample
//
//  Created by Divya Munni on 6/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
    NSManagedObjectContext *context;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self firstnameText]setDelegate:self];
    [[self lastnameText]setDelegate:self];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    context = [appDelegate managedObjectContext];
    
    
    // above two lines to make sure the keyboard hides when we hit enter
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPersonButton:(id)sender {
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSManagedObject *newPerson = [[NSManagedObject alloc] initWithEntity:entityDesc insertIntoManagedObjectContext:context];
    
    [newPerson setValue:self.firstnameText.text forKey:@"firstName"];
    [newPerson setValue:self.lastnameText.text forKey:@"lastName"];
    
    NSError *error;
    [context save:&error]; // this saves a person to the database
    
     NSEntityDescription *entityDesc1 = [NSEntityDescription entityForName:@"Dancer" inManagedObjectContext:context];
     NSManagedObject *newDancer = [[NSManagedObject alloc] initWithEntity:entityDesc1 insertIntoManagedObjectContext:context];
    // int myInt = 13;
    NSNumber *count = @13;
    int value = [count intValue];
    count = [NSNumber numberWithInt:value + 1];
    
    [newDancer setValue:count forKey:@"counter"];
    [newDancer setValue:@"Divya" forKey:@"name"];
    
    NSError *error1;
    [context save:&error1];
}

- (IBAction)searchPersonButton:(id)sender {
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    //can replace with == to get exact match. like with do regex match??
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName like %@ and lastName like %@",self.firstnameText.text, self.lastnameText.text];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0){
        self.statusLabel.text = @"no person found";
    }
    else{
        NSString *firstName;
        NSString *lastName;
        
        for(NSManagedObject *obj in matchingData){
            firstName = [obj valueForKey:@"firstName"];
            lastName = [obj valueForKey:@"lastName"];
        }
        
        self.statusLabel.text = [NSString stringWithFormat:@"Firstname : %@, Lastname : %@", firstName, lastName];
    }
    
}

- (IBAction)deletePersonButton:(id)sender {
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName like %@",self.firstnameText.text];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <=0){
        self.statusLabel.text = @"No person deleted";
    }else {
        for(NSManagedObject *obj in matchingData){
            [context deleteObject:obj];
        }
        [context save:&error];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
} // keyboard hidden when we hit enter

@end
