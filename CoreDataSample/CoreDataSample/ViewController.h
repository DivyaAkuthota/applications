//
//  ViewController.h
//  CoreDataSample
//
//  Created by Divya Munni on 6/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstnameText;
@property (weak, nonatomic) IBOutlet UITextField *lastnameText;

- (IBAction)addPersonButton:(id)sender;
- (IBAction)searchPersonButton:(id)sender;
- (IBAction)deletePersonButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end

