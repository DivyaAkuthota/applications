//
//  Person.h
//  CoreDataSample
//
//  Created by Divya Munni on 6/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic,strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

@end
