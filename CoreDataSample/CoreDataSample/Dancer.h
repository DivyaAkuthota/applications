//
//  Dancer.h
//  CoreDataSample
//
//  Created by Divya Munni on 6/13/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dancer : NSObject
@property (nonatomic, assign) NSNumber *counter;
@property (nonatomic, strong) NSString *name;
@end
