#include<iostream>

using namespace std;

int Fibonacci(int index){
	int first, second, sum, i;
	first = 0;
	second = 1;

	for(i = 2 ; i <= index; i++){
		sum = first +second;
		first = second;
		second = sum;
	}
	return sum;
}


int main(){
	int index;
	cout<<"Enter the index for the Fibonacci number"<<endl;
	cin>>index;
	cout<<"Fibonacci number : "<<Fibonacci(index)<<endl;
	return 0;
}