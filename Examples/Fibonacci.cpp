#include<iostream>

using namespace std;

int Fibonacci(int index) { 
  if (index == 0) return 0;  // Stopping conditions 
  if (index == 1) return 1; 
  return Fibonacci(index - 1) + Fibonacci(index - 2); 
}


int main() { 
  int index;

  cin >> index; 
  cout << Fibonacci(index) << endl;
//  cout << FibonacciIterate(index)<<endl;
  return 0; 
} 