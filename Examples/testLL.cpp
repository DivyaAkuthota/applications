#include<iostream>
#include<string.h>

using namespace std;

class Node{
public:
	int data;
	Node *next;
};

class LinkedList{
public:
	Node *head;
	LinkedList(){
		head = NULL;
	}
	LinkedList(int value){
		head -> data = value;
		head -> next = NULL;
	}
	
	void insertNodeAtEnd(int value);
	int insertAtPosition(int position, int value);
	void listAllNodes();
	int countListItems();
	void deleteAtEnd();
	void deleteValue(int value);
	void deleteAtPosition(int position);
	void reverseList(Node *rev);
	void removeDuplicates();
};

void LinkedList::insertNodeAtEnd(int value){
		Node *tempNode = new Node();
		Node *travNode = head;
		tempNode -> data = value;
		tempNode -> next = NULL;
		if(head == NULL)
		{
			head = tempNode;
		}
		else
		{
			while(travNode -> next != NULL){
				travNode = travNode -> next;
				}
			travNode -> next = tempNode;
		}
		cout<<"added value = "<<value<<endl;
	}


int LinkedList::insertAtPosition(int position, int value){
		Node *travNode = head;
		int count = countListItems();
		if(position == count){
			insertNodeAtEnd(value);
			return 1;
		}
		else if(position > count || position < 0){
			return 0;
		}
		else {
			Node *tempNode = new Node();
			tempNode -> data =value;
			tempNode -> next = NULL;
			if(position == 0)
			{
				tempNode -> next = head;
				head = tempNode;
			}
			else if(position == 1)
			{
				head -> next = tempNode;
			}
			else{
				for(int i=0; i < (position-1); i++)
				{
					travNode = travNode -> next;
				}
				tempNode ->next = travNode -> next;
				travNode -> next = tempNode;
				return 1;
			}
		}

		return 0;
	}

void LinkedList::listAllNodes(){
		cout<<"list all nodes method"<<endl;
		Node *travNode = head;
		while(travNode != NULL){
			cout<<travNode->data<<endl;
			travNode = travNode -> next;
		}
	}

int LinkedList::countListItems(){
		Node *travNode = head;
		int count = 0;
		while(travNode != NULL){
			count++;
			travNode = travNode -> next;
		}
		return count;
	}

void LinkedList::deleteAtEnd(){
	if(head == NULL){
		cout<<"LinkedList is empty"<<endl;
		return;
	}
	else if(head -> next == NULL){
		Node *tempNode = head;
		head = NULL;
		delete tempNode;
		cout<<"Deleted successfully"<<endl;
		return;
	}
	else {
	Node *travNode = head;
	while(travNode->next->next != NULL){
		travNode = travNode -> next;
	}
	Node *tempNode = travNode -> next;
	travNode -> next = NULL;
	delete tempNode;
	cout<<"Deleted successfully"<<endl;
	return;
	}
}

void LinkedList::deleteValue(int value){
	
	if(head == NULL)
	{
		cout<<"List is empty"<<endl;
		return;
	}
	else if(head -> next == NULL){
		if(head -> data == value){
			deleteAtEnd();
			return;
		}
	}
	else{
		cout<<"enters"<<endl;
		Node *travNode = head;
		while(travNode -> next != NULL)
		{
			if(travNode -> data == value)
			{
				travNode -> data = travNode -> next -> data;
				Node *tempNode = travNode -> next;
				travNode -> next = travNode -> next -> next;
				delete tempNode;
				cout<<"successfully deleted"<<endl;
				return;
			}
			travNode = travNode -> next;
		}
	}
}

void LinkedList::deleteAtPosition(int position){
	int count = countListItems();
	if(head == NULL){
		cout<<"head == null"<<endl;
		return;
	}
	else if(position >= count || position < 0){
		cout<<"position >= count || position < 0"<<endl;
		return;
	}
	else{
		Node *travNode = head;
		for(int i=1; i < position; i++){
			travNode = travNode -> next;
		}
			Node *tempNode = travNode -> next;
			travNode -> next = tempNode -> next;
			delete tempNode;
			cout<<"Deleted successfully"<<endl;
		
	}
}


void LinkedList::reverseList(Node *rev){
	if(rev == NULL){
		cout<<"Empty List"<<endl;
		return;
	}
	if(rev -> next == NULL){
		head = rev;
		return;
	}
	reverseList(rev -> next);
	Node *revNext = rev -> next;
	revNext -> next = rev;
	rev -> next = NULL;
	return;
}

void LinkedList::removeDuplicates(){
	if(head == NULL){
		return;
	}
	Node *p1, *p2;

	p1 = head;
	while(p1 != NULL){
		p2 = p1 -> next;
		while(p2 != NULL){
			if(p2 -> data == p1 -> data){
				//deleteNode(&p2);
				Node *temp = p2 -> next;
				p2 -> data = temp ->data;
				p2 -> next = temp -> next;
				free(temp);
				continue;
			}
			p2 = p2 -> next;
		}
		p1 = p1 ->next;
	}
}


int main(){
	LinkedList *LL = new LinkedList();
	char code;
	int option;
	int data;
	int pos;
	
	LL -> insertNodeAtEnd(1);
	LL -> insertNodeAtEnd(2);
	LL -> insertNodeAtEnd(3);
	LL -> insertNodeAtEnd(4);
	LL -> insertNodeAtEnd(5);
	LL -> insertNodeAtEnd(1);
	LL -> insertNodeAtEnd(7);
	LL -> insertNodeAtEnd(2);

	do{
	cout<<"1. Insert element"<<endl;
	cout<<"2. Insert element at a position"<<endl;
	cout<<"3. List all elements"<<endl;
	cout<<"4. Count number of items"<<endl;
	cout<<"5. Delete element at end"<<endl;
	cout<<"6. Delete element at position"<<endl;
	cout<<"7. Delete given element "<<endl;
	cout<<"8. Reverse the list "<<endl;
	cout<<"9. Remove Duplicates "<<endl;
	cout<<"enter 1-10 "<<endl;
	cin>>option;
	switch(option){
		case 1:
				cout<<"Enter number to be added to the list"<<endl;
				cin>>data;
				LL -> insertNodeAtEnd(data);
				break;
		case 2:
				cout<<"Enter position and value that needs to be inserted"<<endl;
				cin>>pos;
				cin>>data;
				if(LL->insertAtPosition(pos,data) == 0){
					cout<<"Please enter valid numbers"<<endl;
				}
				else{
					cout<<"Value inserted successfully"<<endl;
				}
				break;
		case 3:
				cout<<"Listing all the nodes"<<endl;
				LL -> listAllNodes();
				break;
		case 4:
				cout<<"number of items : "<<LL->countListItems()<<endl;
				break;
		case 5:
				LL -> deleteAtEnd();
				break;
		case 6:
				cout<<"Enter position of the element to be deleted"<<endl;
				cin>>pos;
				LL -> deleteAtPosition(pos);
				break;
		case 7:
				cout<<"Enter element to be deleted"<<endl;
				cin>>data;
				LL -> deleteValue(data);
				break;
		case 8:
				LL -> reverseList(LL -> head);
				break;
		case 9:
				LL -> removeDuplicates();
				break;
		default:
				cout<<"Please Enter valid numbers from 1 - 4 "<<endl;
				break;
	}
	cout<<"Do you want to continue?"<<endl;
	cin>>code;
	}while(code == 'y'); 

return 0;
}