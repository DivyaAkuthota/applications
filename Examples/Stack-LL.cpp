#include<iostream>
#include<string.h>

using namespace std;

class Node {
public:
	int data;
	Node *next;
};

class StackLL {
public:
	Node *top;

	StackLL();
	void push(int value);
	int pop();
	bool isEmpty();
};

StackLL::StackLL(){
	top = NULL;
}

void StackLL::push(int value){
	Node *temp = new Node();
	temp -> data = value;
		if(top == NULL){
			temp -> next = NULL;
			top = temp;
			return;
	}
	temp -> next = top;
	top = temp;
	return;
}

int StackLL::pop(){
	if(isEmpty()){
		cout<<"Stack is empty"<<endl;
		return 0;
	}
	Node *temp = top;
	top = top -> next;
	temp -> next = NULL;
	return temp -> data;	
}

bool StackLL::isEmpty(){
	if(top == NULL)
		return true;
	else
		return false;
}


int main(){
	StackLL *SL = new StackLL();
	int option, data;
	char code;
	cout<<"Stack using LinkedList"<<endl;

	do{
		cout<<"1. push"<<endl;
		cout<<"2. pop"<<endl;
		cin>>option;
		switch(option){
			case 1: cout << "Enter Element to be pushed"<<endl;
					cin >> data;
					SL -> push(data);
					break;
			case 2: data = SL -> pop();
					cout<<"Popped Element "<<data<<endl;
					break;
			default:
					cout<<"Enter valid numbers 1 - 2"<<endl;
					break;
		}
		cout<<"Do you want to continue?"<<endl;
		cin>>code;
	}while(code == 'y'); 
	return 0;
}