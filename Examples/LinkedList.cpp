#include<iostream>

using namespace std;


class Node {
public:
	int data;
	Node *next;

};

class LinkedList {
public:
	Node *head;

	// Constructor 
	LinkedList(){
		head = NULL;
	}

	LinkedList(int value) {
		head->next = NULL;
		head->data = value;
	}

	// basic methods 
	void insert(int value);
	void insertPosition(int position, int value);
	void insertStart(int value);
	int deletePosition(int position);
	int deleteValue(int value);
	int deleteEnd();
	int deleteStart();
	void lookupItems();
	int countItems();
	void reverseList();

};

void LinkedList::insert(int value){
	Node *tempNode = new Node();
	tempNode -> data = value;
	tempNode -> next = NULL;

	if(head == NULL){
		head = tempNode;
		cout<<"insert"<<value<<endl;
	}
	else {
		Node *travNode = head;
		while(travNode -> next != NULL){
			travNode = travNode -> next;
		} // traverse until the last node
			travNode -> next = tempNode;
		cout<<"insert"<<value<<endl;
		
	}
}


void LinkedList::insertPosition(int position, int value){
	Node *travNode = head;
	if(position > countItems())
		return;
	if(head == NULL)
		insert(value);
	else if(head -> next == NULL) {
			Node *tempNode = new Node();
			tempNode -> data = value;
			tempNode -> next = NULL;
			if(position == 0)
			{
				tempNode -> next = head;

				head = tempNode;
			}
			else if(position == 1)
			{
				head -> next = tempNode;
			}
	}
	else {
			Node *tempNode = new Node();
			tempNode -> data = value;
			tempNode -> next = NULL;
			for(int i=0; i < position; i++)
			{
			travNode = travNode -> next;
			}
			tempNode -> next = travNode -> next;
			travNode -> next = tempNode;
	}
}


void LinkedList::lookupItems() {
	Node *travNode = head;
	while(travNode != NULL){
		cout<<travNode->data<<endl;
		travNode = travNode -> next;
	}
}

int LinkedList::countItems() {
	int count = 0;
	Node *travNode = head;
	while(travNode != NULL){
		count++;
		travNode = travNode -> next;
	}
	return count;
}


int main(){
	LinkedList *LL = new LinkedList();
	LL->insert(10);
	LL->insertPosition(0,50);
	LL->insert(20);
	LL->insert(30);

	LL->lookupItems();
return 0;
}