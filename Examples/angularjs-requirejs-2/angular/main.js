// appDemo.controller('dialogServiceTest',function($scope,$rootScope,$timeout,$dialogs){

  var dialogServiceTest = function($scope,$rootScope,$timeout,$dialogs){

  $scope.confirmed = 'You have yet to be confirmed!';
  $scope.name = '"Your name here."';

  $scope.launch = function(){
    var dlg = null;
      // Create Your Own Dialog

        dlg = $dialogs.create('/dialogs/whatsyourname.html','whatsYourNameCtrl',{},{key: false,back: 'static'});
        dlg.result.then(function(name){
          $scope.name = name;
        },function(){
          $scope.name = 'You decided not to enter in your name, that makes me sad';
        });

  }; // end launch
/*
// end dialogsServiceTest
 .controller('whatsYourNameCtrl',function($scope,$modalInstance,data){
  $scope.user = {name : ''};

  $scope.cancel = function(){
    $modalInstance.dismiss('canceled');
  }; // end cancel

  $scope.save = function(){
    $modalInstance.close($scope.user.name);
  }; // end save

}) // end whatsYourNameCtrl
.run(['$templateCache',function($templateCache){
  $templateCache.put('/dialogs/whatsyourname.html','<div> function custom </div>');
}]); // end run / module
 */

appDemo.controller("dialogServiceTester",dialogServiceTester);
