var appDemo = angular.module("demoModule",[]);


  var DemoController = function($scope,$window){

      $scope.hellomsg = "my web page shines";
      $scope.dis = "hello templatee";

      $scope.clickme = function(){
        $window.alert("displayyyy");

      };

        $scope.launch = function(){
          var dlg = null;
            // Create Your Own Dialog

              dlg = $dialogs.create('/dialogs/whatsyourname.html','whatsYourNameCtrl',{},{key: false,back: 'static'});
              dlg.result.then(function(name){
                $scope.name = name;
              },function(){
                $scope.name = 'You decided not to enter in your name, that makes me sad';
              });

        }; // end launch

  };

  var whatsYourNameCtrl = function($scope,$modalInstance,data){

    $scope.user = {name : ''};

    $scope.cancel = function(){
      $modalInstance.dismiss('canceled');
    }; // end cancel

    $scope.save = function(){
      $modalInstance.close($scope.user.name);
    }; // end save
  }


appDemo.controller("DemoController",DemoController);
