#include<iostream>

using namespace std;

class node
{   
    public:
    int data;
    node *next;
};


class LinkedList
{   
    public:
        node *head, *tail;
    
        void insert(int val);
        void lookup();
        void deletepos(int pos);
        void deleteval(int val);
        void insertIndex(int index);
        void insertStart(int value);
        void insertEnd(int value);
        void deletestart();
        void deleteEnd();
        int count();
        void reverseList();

LinkedList()
{
    head = tail = NULL;
}
};


void LinkedList::insert(int value)
{
    if(head == NULL && tail == NULL)
    {
        cout<<"empty list"<<endl;
        node *n = new node();
        n->data = value;
        n->next = NULL;
        head = n;
        tail = n;
    }
    else
    {
        cout<<"inserting.. "<<value<<endl;
        node *n = new node();
        n->data = value;
        n->next = NULL;
         
        tail->next = n;
        tail = n;
    }
}


void LinkedList::reverseList()
{
    node *ptr = head;
    node *temp , *prev = NULL;

    while(ptr != NULL)
    {
        temp = ptr->next;
        ptr->next = prev;
        prev = ptr;
        ptr = temp;
    }
    head = prev;
    
}


void LinkedList::insertStart(int value)
{

}


void LinkedList::insertEnd(int value)
{

}

void LinkedList::deletestart()
{
    node *todelete = head;
    head = head->next;
    delete todelete;
}


void LinkedList::deleteEnd()
{
    node *trav = head;

    while(trav->next == tail)
    {
        trav=trav->next;
    }

    node *todelete = tail;
    trav->next = NULL;
    delete todelete;
}


int LinkedList::count()
{
    node *trav = head;
    int count = 0;
    while(trav != NULL)
    {
        count++;
        trav = trav->next;
    }
    return count;
}


void LinkedList::insertIndex(int index)
{

}

// positions from 1 to n
void LinkedList::deletepos(int position)
{
    if(head != NULL && tail != NULL)
    {
    node *trav = head;
    
        for(int i = 1; i < position - 1; i++)
        {
            trav = trav->next;
        }
    node *forward = trav->next->next;
    node *todelete = trav->next;
    trav->next = forward;
    delete todelete;
    }
    else if(head == tail)
    {
        cout<<"there is just one node"<<endl;
        node *todelete = head;
        head = tail = NULL ;
        delete todelete;
    }
    else
    {
        cout<<"there are no nodes to delete"<<endl;
    }
}


void LinkedList::deleteval(int value)
{
    node *trav = head;
    if(trav->data == value) // head node deletion
    {
        node *todelete = head;
        head = head->next;
        delete todelete;
    }
    else if(tail->data == value)
    {
        while(trav->next != tail)
        {
            trav = trav->next;
        }
        node *todelete = tail;
        trav->next = NULL;
        delete todelete;
    }
    else
    {
        while(trav->next->data != value)
        {
            trav = trav->next;
        }   
        node *forward = trav->next->next;
        node *todelete = trav->next;
        trav->next = forward;
        delete todelete;
    }

}


void LinkedList::lookup()
{
    node *trav = head;
    int count = 0;
    while(trav != NULL)
    {
        count++;
        cout<<(trav->data)<<endl;
        trav = trav->next;
    }
    cout<<count<<endl;
}

int main()
{
    LinkedList ll;
    int i;
    int element;
//    do
  //  {
    cout<<"Select an option:"<<endl;
    cout<<"1.insert an element"<<endl;
    cout<<"2.insert at the beginning"<<endl;
    cout<<"3.insert at the end"<<endl;
    cout<<"4.delete at the given position"<<endl;
    cout<<"5.delete at the beginning"<<endl;
    cout<<"6.delete at the end"<<endl;
    cout<<"7.count the number"<<endl;
    cout<<"8.lookup the elements"<<endl;
    cout<<"9.reverse the list"<<endl;
    cout<<"10.delete the given value"<<endl;
    //cin>>i;
   /* switch(i)
    {
        case 1: 
            cout<<"enter an element to insert"<<endl;
            cin>>element;
            ll.insert(element);
            break;
        case 2:
            cout<<"enter an element to insert"<<endl;
            cin>>element;
            ll.insertStart(element);
            break;
        case 3:
            cout<<"enter an element to insert"<<endl;
            cin>>element;
            ll.insertEnd(element);
            break;
        case 4:
            cout<<"enter the position to be deleted"<<endl;
            cin>>element;
            ll.deletepos(element);
            break;
        case 5:
            ll.deletestart();
            break;
        case 6:
            ll.deleteEnd();
            break;
        case 7:
            int result = ll.count();
            cout<<"the number of nodes are : "<<result<<endl;
            break;
        case 8:
            ll.lookup();
            break;
        case 9:
            ll.reverseList();
            break;
        case 10:
            cout<<"enter the value to be deleted"<<endl;
            cin>>element;
            ll.deleteval(element);
            break;
        default:
            cout<<"no option selected"<<endl;
    }
    } while(true);*/
    //ll.insert(11);
    ll.insert(22);
    ll.insert(33);
    ll.insert(44);
    ll.insert(55);
    ll.insert(66);
    ll.insert(77);
    //ll.insert(88);
    //ll.insert(99);
    //
    ll.lookup();

    ll.reverseList();
    ll.lookup();
    return 0;
}
