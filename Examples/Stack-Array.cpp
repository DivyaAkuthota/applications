#include<iostream>
#include<string.h>

#define SIZE 4

using namespace std;

class StackArray {
public:
	int top;
	int stack[SIZE];

	StackArray();

	void push(int value);
	int pop();
	bool isEmpty();
	bool isFull();
};

StackArray::StackArray(){
	top = -1;
}

void StackArray::push(int value){
	if(isFull()){
		cout<<"Stack is full"<<endl;
		return;
	}
	top++;
	stack[top] = value;
	cout<<"pushed element : "<<value<<endl;
	return;
}

int StackArray::pop(){
	if(isEmpty()){
		cout<<"Stack is empty"<<endl;
		return 0;
	}
	int value = stack[top];
	top--;
	return value;
}

bool StackArray::isEmpty(){
	if(top == -1)
		return true;
	else
		return false;
}

bool StackArray::isFull(){
	if(top == SIZE-1)
		return true;
	else
		return false;
}


int main()
{
	StackArray *SA = new StackArray();
	int option, data;
	char code;
	cout<<"Stack using Array "<<endl;

	do{
		cout<<"1. push"<<endl;
		cout<<"2. pop"<<endl;
		cin>>option;
		switch(option){
			case 1: cout << "Enter Element to be pushed"<<endl;
					cin >> data;
					SA -> push(data);
					break;
			case 2: data = SA -> pop();
					cout<<"Popped Element "<<data<<endl;
					break;
			default:
					cout<<"Enter valid numbers 1 - 2"<<endl;
					break;
		}
		cout<<"Do you want to continue?"<<endl;
		cin>>code;
	}while(code == 'y'); 

	return 0;
}