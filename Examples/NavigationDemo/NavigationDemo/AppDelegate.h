//
//  AppDelegate.h
//  NavigationDemo
//
//  Created by Panda, Srinivas on 5/6/14.
//  Copyright (c) 2014 MerrillLynch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
