//
//  WMT_CF_ADP_ModalViewController.m
//  NavigationDemo
//
//  Created by Divya Munni on 6/10/14.
//  Copyright (c) 2014 MerrillLynch. All rights reserved.
//

#import "WMT_CF_ADP_ModalViewController.h"

@interface WMT_CF_ADP_ModalViewController ()
- (IBAction)ContinueButton:(id)sender;
- (IBAction)CancelButton:(id)sender;


@end

@implementation WMT_CF_ADP_ModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ContinueButton:(id)sender {
    NSLog(@"continue click in ModalViewController");
}

- (IBAction)CancelButton:(id)sender {
    NSLog(@"cancel click in ModalViewController");
}
@end
