//
//  ViewController1.m
//  NavigationDemo
//
//  Created by Panda, Srinivas on 5/6/14.
//  Copyright (c) 2014 MerrillLynch. All rights reserved.
//

#import "WMT_CF_ADP_ViewController.h"
#import "WMT_CF_PDP_ViewController.h"

@interface WMT_CF_ADP_ViewController () {
    UIActivityIndicatorView *activityIndicator;
}

@property (weak, nonatomic) IBOutlet UIWebView *webView1;

@end

@implementation WMT_CF_ADP_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(0.0, 0.0, 50.0, 50.0);
    //activityIndicator.backgroundColor=[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.5];
    activityIndicator.center=self.navigationController.view.center;
    [activityIndicator startAnimating];
    [self.view addSubview:activityIndicator];
    
    UIBarButtonItem *continueButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Continue"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(continueAction:)];
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Close"
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(closeAction:)];
    self.navigationItem.rightBarButtonItem = continueButton;
    self.navigationItem.leftBarButtonItem = closeButton;
    
    NSString *urlAddress = @"http://www.google.com";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView1 loadRequest:requestObj];
    _webView1.delegate=self;
}

-(void)continueAction:(UIBarButtonItem *)sender{
    
    NSLog(@"Continue is tapped in VC1");
    
    WMT_CF_PDP_ViewController *vc2 = [[WMT_CF_PDP_ViewController alloc] initWithNibName:@"WMT_CF_PDP_ViewController" bundle:nil];
    [self.navigationController pushViewController:vc2 animated:YES];
}

-(void)closeAction:(UIBarButtonItem *)sender{
 
    NSLog(@"Close is tapped in VC1");
    
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [activityIndicator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [activityIndicator stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error while loading the page" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
