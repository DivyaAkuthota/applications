//
//  MainViewController.m
//  NavigationDemo
//
//  Created by Panda, Srinivas on 5/6/14.
//  Copyright (c) 2014 MerrillLynch. All rights reserved.
//

#import "MainViewController.h"
#import "WMT_CF_ADP_ViewController.h"
#import "WMT_CF_ADP_ModalViewController.h"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UIButton *buttonModalWindow;
-(IBAction)modalWindowClicked:(id)sender;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)modalWindowClicked:(id)sender {
    
    WMT_CF_ADP_ViewController *vc1 = [[WMT_CF_ADP_ViewController alloc] initWithNibName:@"WMT_CF_ADP_ViewController" bundle:nil];
    
    WMT_CF_ADP_ModalViewController *mvc = [[WMT_CF_ADP_ModalViewController alloc] initWithNibName:@"WMT_CF_ADP_ModalViewController" bundle:nil];
    
    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(25, 62, 320, 480)];
    
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mvc];
    navView.clipsToBounds = YES;
    [navView addSubview:navController.view];
    

    [navController pushViewController:vc1 animated:NO];
    navController.view.frame = CGRectMake(20,20,100,100);
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navController animated:NO completion:nil];
    [self.view addSubview:navView];
    
   }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
