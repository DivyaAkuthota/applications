//
//  Product.h
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, assign) float unitPrice;

- (id)initWithName:(NSString*)n andDescription:(NSString*)d andUnitPrice:(int)p;

@end
