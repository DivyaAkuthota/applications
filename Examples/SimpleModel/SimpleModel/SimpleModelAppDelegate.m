//
//  SimpleModelAppDelegate.m
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import "SimpleModelAppDelegate.h"

#import "Product.h"
#import "RootViewController.h"

@implementation SimpleModelAppDelegate

@synthesize window = _window;
@synthesize navigationController = _navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*
     Model initialization: the app delegate allocate and initialize the model (i.e. an array of products).
     The reference to the array (or to a single product) will be passed from view controller to view controller
    */
    products = [[NSMutableArray alloc] init];
    
    // Here you should load your model
    Product *p1 = [[Product alloc] initWithName:@"Gold" andDescription:@"Expensive metal" andUnitPrice:100];
    Product *p2 = [[Product alloc] initWithName:@"Wood" andDescription:@"Inexpensive building material" andUnitPrice:10];
    
    [products addObject:p1];
    [products addObject:p2];
    
    // NSMutableArray retains the reference for us, so we can release p1 and p2
    [p1 release];
    [p2 release];
    
    /*
     Passing the reference to view controllers.
     
     The root view controller is a UINavigationController, which doesn't support the addition of ivars or methods.
     http://stackoverflow.com/questions/1937616/why-doesnt-apple-allow-subclassing-of-uinavigationcontroller-and-what-are-my-al
     
     We will pass the reference to the root controller, which is the one in viewControllers[0].
     Do *not* use topViewController property, which points to the visible view controller!
    */
     
    RootViewController *a = (RootViewController*)[self.navigationController.viewControllers objectAtIndex:0];
    a.products = products;
    
    // Add the navigation controller's view to the window and display
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)dealloc
{
    // The app delegate is the owner of the model so it has to release it.
    [products release];
    [_window release];
    [_navigationController release];
    
    [super dealloc];
}

@end
