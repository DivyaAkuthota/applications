//
//  DetailViewController.h
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@interface DetailViewController : UIViewController

// *product contains the part of the model that the controller needs
@property (nonatomic, retain) Product *product;

@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UILabel *priceLabel;

@end
