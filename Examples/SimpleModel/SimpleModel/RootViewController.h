//
//  RootViewController.h
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController

@property (nonatomic, retain) NSMutableArray *products;

@end
