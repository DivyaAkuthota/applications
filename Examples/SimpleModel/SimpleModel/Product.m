//
//  Product.m
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize name, description, unitPrice;

- (id)initWithName:(NSString*)n andDescription:(NSString*)d andUnitPrice:(int)p
{
    self = [super init];
    if (self) {
        self.name = n;
        self.description = d;
        self.unitPrice = p;
    }
    
    return self;
}

- (id)init
{
    return [self initWithName:@"None" andDescription:@"None" andUnitPrice:-1];
}


- (void)dealloc
{
    [name release];
    [description release];
    
    [super dealloc];
    
}

@end
