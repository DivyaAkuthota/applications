//
//  SimpleModelAppDelegate.h
//  SimpleModel
//
//  Created by Andrea on 8/25/11.
//  Copyright 2011 Andrea Rota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleModelAppDelegate : NSObject <UIApplicationDelegate>
{
    @protected
    NSMutableArray *products;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;


@end
