//
//  ViewController.h
//  TestCamera
//
//  Created by Divya Munni on 6/30/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>

@interface ViewController : UIViewController  <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)takePhoto:(id)sender;
- (IBAction)selectPhoto:(id)sender;

-(void)checkForDeviceStatus;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *cameraActivityIndicator;

@end

