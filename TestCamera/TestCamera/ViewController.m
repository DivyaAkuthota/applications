//
//  ViewController.m
//  TestCamera
//
//  Created by Divya Munni on 6/30/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


CKContainer *defaultContainer;
CKDatabase *privateDatabase;
CKDatabase *publicDatabase;


- (void)viewDidLoad {
    [super viewDidLoad];
    defaultContainer = [CKContainer defaultContainer];
    publicDatabase = [defaultContainer publicCloudDatabase];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) viewDidAppear:(BOOL)animated{
    [self checkForDeviceStatus];
}

-(void)checkForDeviceStatus {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Device has no camera" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takePhoto:(id)sender {
    [self.cameraActivityIndicator startAnimating];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    [self.cameraActivityIndicator stopAnimating];
}

- (IBAction)selectPhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    CKRecord *postImage = [[CKRecord alloc] initWithRecordType:@"ImageList"];
//    postImage[@"picture"] = self.imageView.image;
    [publicDatabase saveRecord:postImage completionHandler:^(CKRecord *record, NSError *error){
        if(error){
            NSLog(@"not saved");
        }else{
            NSLog(@"Image successfully saved");
        }
    }];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
