//
//  ViewController.h
//  TestRealEstate
//
//  Created by Divya Munni on 6/26/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@end

