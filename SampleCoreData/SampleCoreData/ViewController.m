//
//  ViewController.m
//  SampleCoreData
//
//  Created by Divya Munni on 3/8/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
NSManagedObjectContext *context;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // below 2 lines to tackle the keyboard issue
    [[self firstNameTextField]setDelegate:nil];
    [[self lastNameTextField]setDelegate:nil];
    
    AppDelegate *appdelegate = [[UIApplication sharedApplication]delegate];
    context = [appdelegate managedObjectContext];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addPersonButton:(id)sender {
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSManagedObject *newPerson = [[NSManagedObject alloc]initWithEntity:entityDesc insertIntoManagedObjectContext:context];
    [newPerson setValue:self.firstNameTextField.text forKey:@"firstname"];
    [newPerson setValue:self.lastNameTextField.text forKey:@"lastname"];
    self.displayLabelTextField.text = @"Person added";
    
    NSError *error;
    
    [context save:&error];
}

- (IBAction)searchPersonButton:(id)sender {
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Person" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDesc];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstname like %@ and lastname like %@",self.firstNameTextField.text, self.lastNameTextField.text];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    
    if(matchingData.count <= 0){
        self.displayLabelTextField.text = @"Person not found";
    }
    else{
        NSString *firstName;
        NSString *lastName;
        
        for(NSManagedObject *obj in matchingData){
            firstName = [obj valueForKey:@"firstname"];
            lastName = [obj valueForKey:@"lastname"];
        }
        self.displayLabelTextField.text = [NSString stringWithFormat:@"person found"];
    }
    
}

- (IBAction)deletePersonButton:(id)sender {
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder]; // when we hit the return key keyboard will be hidden
}
@end
