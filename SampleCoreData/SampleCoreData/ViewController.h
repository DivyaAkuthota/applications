//
//  ViewController.h
//  SampleCoreData
//
//  Created by Divya Munni on 3/8/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *displayLabelTextField;

- (IBAction)addPersonButton:(id)sender;
- (IBAction)searchPersonButton:(id)sender;
- (IBAction)deletePersonButton:(id)sender;


@end

