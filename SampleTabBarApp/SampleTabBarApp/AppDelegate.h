//
//  AppDelegate.h
//  SampleTabBarApp
//
//  Created by Divya Munni on 2/29/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

