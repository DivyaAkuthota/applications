//
//  MasterViewController.h
//  BugsList
//
//  Created by Divya Munni on 2/25/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

