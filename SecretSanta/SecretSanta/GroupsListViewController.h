//
//  GroupsListViewController.h
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SantaTabBarController.h"

@interface GroupsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource> 

@property (weak, nonatomic) IBOutlet UITableView *GroupsListTableView;

@end
