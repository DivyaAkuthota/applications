//
//  BaseTabBarController.h
//  SecretSanta
//
//  Created by Divya Munni on 3/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTabBarController : UITabBarController



-(UIViewController *) viewControllerWithTabTitle:(NSString *)title image:(UIImage *)image;

-(void) addCenterButtonWithImage:(UIImage *)buttonImage highlightImage:(UIImage *)highlightImage;


@end
