//
//  GroupsListViewController.m
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "GroupsListViewController.h"

@interface GroupsListViewController ()

@end

@implementation GroupsListViewController

NSArray *groupList;

- (void)viewDidLoad {
    [super viewDidLoad];
      groupList = [NSArray arrayWithObjects:@"Divya", @"Navya", @"Anirudh", @"Mounika",@"Nikhila",@"Rupali",nil];
    self.GroupsListTableView.rowHeight = 60;
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [groupList count];
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *groupListCell = @"groupListCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:groupListCell];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:groupListCell];
    }
    cell.imageView.image = [UIImage imageNamed:@"child.jpg"];
    cell.textLabel.text = [groupList objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = @"people";
    //Configure the cell...
    
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
