//
//  GroupsViewController.h
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *GroupsTableView;

@end
