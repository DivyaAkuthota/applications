//
//  GroupsViewController.m
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "GroupsViewController.h"
#import "GroupsListViewController.h"

@interface GroupsViewController ()

@end

@implementation GroupsViewController

NSArray *groups;


- (void)viewDidLoad {
    [super viewDidLoad];
    groups = [NSArray arrayWithObjects:@"Santa2000",@"Santa2001",@"Santa2002",@"Santa2003", nil];
    //[self.tabBarController setTitle:@"Title"];
    self.navigationItem.title = @"Santa Groups";
    //self.navigationItem.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.GroupsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
      self.navigationItem.leftBarButtonItem = self.editButtonItem;
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView : (UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    NSString *groupCell = @"GroupCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:groupCell];
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:groupCell];
    }
    
    cell.imageView.image = [UIImage imageNamed:@"hat.png"];
    cell.textLabel.text = [groups objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = @"2000";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.GroupsTableView.rowHeight = 80;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [groups count];
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"openGroupList" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"openGroupList"]){
        NSLog(@"prepareforsegue");
        GroupsListViewController *gvc = (GroupsListViewController *)segue.destinationViewController;
       // [self presentViewController:gvc animated:YES completion:nil];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
