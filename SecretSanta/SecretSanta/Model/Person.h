//
//  Person.h
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Group;

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Person+CoreDataProperties.h"
