//
//  Person+CoreDataProperties.h
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *correlationid;
@property (nullable, nonatomic, retain) NSNumber *groupid;
@property (nullable, nonatomic, retain) Group *partof;

@end

NS_ASSUME_NONNULL_END
