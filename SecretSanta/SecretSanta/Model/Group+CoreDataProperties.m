//
//  Group+CoreDataProperties.m
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Group+CoreDataProperties.h"

@implementation Group (CoreDataProperties)

@dynamic year;
@dynamic name;
@dynamic id;
@dynamic contains;

@end
