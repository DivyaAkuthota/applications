//
//  Person+CoreDataProperties.m
//  SecretSanta
//
//  Created by Divya Munni on 3/10/16.
//  Copyright © 2016 app. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

@dynamic id;
@dynamic name;
@dynamic email;
@dynamic correlationid;
@dynamic groupid;
@dynamic partof;

@end
