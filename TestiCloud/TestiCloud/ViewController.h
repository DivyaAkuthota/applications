//
//  ViewController.h
//  TestiCloud
//
//  Created by Divya Munni on 6/28/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CloudKit.h"
#import <CloudKit/CloudKit.h>


@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UITextField *addText;

@property (strong, nonatomic) IBOutlet UITextField *deleteText;

- (IBAction)didTapRefreshButton:(id)sender;

- (IBAction)didTapAddButton:(id)sender;

- (IBAction)didTapDeleteButton:(id)sender;

@end

