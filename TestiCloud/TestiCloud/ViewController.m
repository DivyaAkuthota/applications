//
//  ViewController.m
//  TestiCloud
//
//  Created by Divya Munni on 6/28/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

CKContainer *defaultContainer;
CKDatabase *privateDatabase;
CKDatabase *publicDatabase;

- (void)viewDidLoad {
    [super viewDidLoad];
    defaultContainer = [CKContainer defaultContainer];
    privateDatabase = [defaultContainer publicCloudDatabase];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapRefreshButton:(id)sender {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"MyList" predicate:predicate];
    [privateDatabase performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error){
        if(!error){
           // NSLog(@"%@", results);
//            CKRecord *ckr;
            for (CKRecord *ckr in results) {
                NSLog(@"%@", [ckr objectForKey:@"item"]);
            }
        }else{
            NSLog(@"%@", error);
        }
    }];
    
}

- (IBAction)didTapAddButton:(id)sender {
    if([self.addText.text  isEqual: @""] || self.addText.text == nil){
        return;
    }
    CKRecord *postRecord = [[CKRecord alloc] initWithRecordType:@"MyList"];
    postRecord[@"item"] = self.addText.text;
    [privateDatabase saveRecord:postRecord completionHandler:^(CKRecord *record, NSError *error){
        if(error){
            NSLog(@"%@", error);
        }else{
            NSLog(@"Saved Successfully");
        }
    }];
    
}

- (IBAction)didTapDeleteButton:(id)sender {
}
@end
