//
//  AppDelegate.h
//  ImageViewClickEvent
//
//  Created by Divya Munni on 6/12/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

