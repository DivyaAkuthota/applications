//
//  ViewController.m
//  ImageViewClickEvent
//
//  Created by Divya Munni on 6/12/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

bool flip;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.clickImage.userInteractionEnabled = YES;
    flip = YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(respondToTapGesture)];
    [self.clickImage addGestureRecognizer:tapRecognizer];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)respondToTapGesture{
    NSLog(@"responded to image tap");
    if(flip == YES){
    self.clickImage.image = [UIImage imageNamed:@"fullGlass-3.png"];
        flip = !flip;
    }else{
        self.clickImage.image = [UIImage imageNamed:@"emptyGlass.png"];
        flip = !flip;
    }
    
}


- (void)tapped:(UITapGestureRecognizer*)tap
    {
        NSLog(@"Image tapped");
    }

@end
