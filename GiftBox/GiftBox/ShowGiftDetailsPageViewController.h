//
//  ShowGiftDetailsPageViewController.h
//  GiftBox
//
//  Created by Divya Munni on 2/28/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowGiftDetailsPageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *giftDetailsLabel;
@property (strong, nonatomic) NSString *giftDetailsString;
@end
