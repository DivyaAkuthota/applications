//
//  ShowGiftDetailsPageViewController.m
//  GiftBox
//
//  Created by Divya Munni on 2/28/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ShowGiftDetailsPageViewController.h"

@interface ShowGiftDetailsPageViewController ()

@end

@implementation ShowGiftDetailsPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"GiftDetails";
    _giftDetailsLabel.text = self.giftDetailsString;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
