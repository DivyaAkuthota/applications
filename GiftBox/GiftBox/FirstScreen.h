//
//  FirstScreen.h
//  GiftBox
//
//  Created by Divya Munni on 2/27/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowGiftDetailsPageViewController.h"

@interface FirstScreen : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *giftBoxTableView;

@property (strong , nonatomic) NSArray *GiftsList;

@end
