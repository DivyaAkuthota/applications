//
//  ViewController.h
//  GiftBox
//
//  Created by Divya Munni on 2/23/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)DisplayText:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *EnterText;

@property (weak, nonatomic) IBOutlet UILabel *ShowText;

@end

