//
//  FirstScreen.m
//  GiftBox
//
//  Created by Divya Munni on 2/27/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "FirstScreen.h"

@interface FirstScreen ()

@end

@implementation FirstScreen

NSArray *tableData;
NSMutableArray *contentArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
    contentArray = [NSMutableArray arrayWithContentsOfFile:path];
    
    self.navigationItem.title = @"List of Gifts";
    
   // tableData = [NSArray arrayWithObjects:@"Egg omlet", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini",@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    // Do any additional setup after loading the view.
}

-(void) insertNewObject {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"EnterItem" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"City"; //for passwords
        textField.secureTextEntry = NO;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"State"; //for passwords
        textField.secureTextEntry = NO;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Description"; //for passwords
        textField.secureTextEntry = NO;
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *city = alert.textFields[0];
        UITextField *state = alert.textFields[1];
        UITextField *description = alert.textFields[2];
        NSArray  *keys = [NSArray arrayWithObjects: @"city", @"state", @"cityText", nil];
        NSDictionary *result = [[NSDictionary alloc] initWithObjects:@[city.text, state.text, description.text] forKeys:keys];
       // [contentArray initWithObjects:result, nil];
        [contentArray addObject:result];
        NSInteger countRows = [contentArray count];
        NSLog(@"no of rows %ld", (long)countRows);
        NSIndexPath *nsIndexPath = [NSIndexPath indexPathForRow:countRows-1 inSection:0];
        [self.giftBoxTableView insertRowsAtIndexPaths:@[nsIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        NSLog(@"%@ %@ %@",city.text,state.text,description.text);
        NSLog(@"%@",contentArray);
    }];
    [alert addAction:okAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    NSLog(@" insertNewObject");
    
    //NSManagedObjectModel *myMO =
    
}

- (void)setEditing:(BOOL)editing
          animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    [self.giftBoxTableView setEditing:editing animated:animated];
}

- (BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        // remove from NSMutableArray
        [contentArray removeObjectAtIndex:indexPath.row];
       
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


- (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"tableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
   // [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
      // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
   // NSLog(@"contentsArray %@", [contentArray objectAtIndex:indexPath.row]);
    // [contentArray objectAtIndex:indexPath.row] gives nsdictionary
    NSArray *allvalues = [[contentArray objectAtIndex:indexPath.row] allValues];
    UIImage *subImage = [UIImage imageNamed:@"Icon@2x.png"];
    cell.imageView.image = subImage;
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = [allvalues objectAtIndex:1];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   // self.giftBoxTableView.rowHeight = 80;
    return cell;
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   // NSLog(@"willselectrowatindexpath %@",[tableData objectAtIndex:indexPath.row]);
    return indexPath;
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    
//}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contentArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"OpenGiftsDetails" sender:self];
    
    
//    
//    NSString *selectedCell = [NSString stringWithFormat:@"You selected %@" , [tableData objectAtIndex:indexPath.row]];
//    
//    UIAlertController *uiAlert = [UIAlertController alertControllerWithTitle:@"Title" message:selectedCell preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OKAY" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
//    [uiAlert addAction:action];
//    [self presentViewController:uiAlert animated:YES completion:nil];
//    
}

/*
 The prepareForSegue:sender: method's sender parameter is the cell you are looking for. You can get it's index path by [self.tableView indexPathForCell:sender] (you may need a cast).
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"OpenGiftsDetails"]){
        ShowGiftDetailsPageViewController *showGift = (ShowGiftDetailsPageViewController *)segue.destinationViewController;
        //showGift.giftDetailsString = [tableData objectAtIndex:1];
        NSIndexPath *path = [self.giftBoxTableView indexPathForSelectedRow];
        if(path.section == 0){
       // NSArray *allvalues = [[contentArray objectAtIndex:path.row] allValues];
            NSString *valueForKey = [[contentArray objectAtIndex:path.row] objectForKey:@"cityText"];
            NSLog(@"%@",valueForKey);
       // NSLog(@"tableview %@", [allvalues objectAtIndex:0]);
            showGift.giftDetailsString = valueForKey;
        }
    }
}
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    // Assume self.view is the table view
//    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
//    DetailObject *detail = [self detailForIndexPath:path];
//    [segue.destinationViewController setDetail:detail];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
