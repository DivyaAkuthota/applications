//
//  AppDelegate.h
//  TabBarCenterButton
//
//  Created by Divya Munni on 3/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

