//
//  MainViewController.h
//  TabBarCenterButton
//
//  Created by Divya Munni on 3/11/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
NSArray *tableViewData;
}
@property (weak, nonatomic) IBOutlet UITableView *AppListTableView;

@end
