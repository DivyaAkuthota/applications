//
//  ViewController.h
//  CoffeeCalculator
//
//  Created by Divya Munni on 2/15/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *waterTextField;
@property (weak, nonatomic) IBOutlet UITextField *ratioTextField;
@property (weak, nonatomic) IBOutlet UITextField *coffeeTextField;

- (IBAction)calculateButtonPressed:(id)sender;

@end

