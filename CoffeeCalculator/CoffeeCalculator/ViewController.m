//
//  ViewController.m
//  CoffeeCalculator
//
//  Created by Divya Munni on 2/15/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)calculateButtonPressed:(id)sender {
    
    NSLog(@"Calculate button pressed");
    
    float water = [[self.waterTextField text] floatValue];
    float ratio = [[self.ratioTextField text] floatValue];
    float coffee = water/ratio;
    
    NSString *coffeeString = [NSString stringWithFormat:@"%f", coffee];
    self.coffeeTextField.text = coffeeString;
}
@end
