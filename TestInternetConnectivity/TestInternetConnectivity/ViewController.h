//
//  ViewController.h
//  TestInternetConnectivity
//
//  Created by Divya Munni on 6/30/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "SystemConfiguration/SystemConfiguration.h"

@interface ViewController : UIViewController
{
Reachability* internetReachable;
Reachability* hostReachable;
}
-(void) checkNetworkStatus:(NSNotification *)notice;


@end

