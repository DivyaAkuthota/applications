//
//  ViewController.h
//  SimpleTableViewApp
//
//  Created by Divya Munni on 2/25/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *sampleTableView;

@end

