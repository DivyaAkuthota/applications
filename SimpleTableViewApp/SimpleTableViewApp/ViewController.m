//
//  ViewController.m
//  SimpleTableViewApp
//
//  Created by Divya Munni on 2/25/16.
//  Copyright © 2016 app. All rights reserved.
//

#import "ViewController.h"
#import "ModalViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSArray *tableData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tableData = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini",@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    //self.sampleTableView.bounces = YES;
    UIImage *image = [UIImage imageNamed:@"side.png"];
    cell.imageView.image = image;
    cell.textLabel.text = tableData[indexPath.row];
    //NSString *indexrow = [NSString stringWithFormat:@"Inside cellforrowat index path - %d",(int)indexPath.row];
    
    //NSLog(indexrow,nil);
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    
    if(indexPath.row < 13){
        cell.detailTextLabel.text = @"First set";
    }else {
        cell.detailTextLabel.text = @"Second set";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Value Selected by user
    //NSString *selectedValue = [tableData objectAtIndex:indexPath.row];
    //Initialize new viewController
    //ModalViewController *mvc = [[ModalViewController alloc] initWithNibName:@"ModalViewController" bundle:nil];
    //Pass selected value to a property declared in NewViewController
   // mvc.valueToPrint = selectedValue;
    //Push new view to navigationController stack
    //[self.navigationController pushViewController:mvc animated:YES];
    NSString *indexrow = [NSString stringWithFormat:@"Inside cellforrowat index path - %d",(int)indexPath.row];
    
    NSString *selectedCell = [NSString stringWithFormat:@"You selected %@" , [tableData objectAtIndex:indexPath.row]];
    NSLog(indexrow,nil);

    UIAlertController *uiAlert = [UIAlertController alertControllerWithTitle:@"Title" message:selectedCell preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OKAY" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    [uiAlert addAction:action];
    [self presentViewController:uiAlert animated:YES completion:nil];
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
