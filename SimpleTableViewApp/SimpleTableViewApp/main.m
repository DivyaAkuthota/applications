//
//  main.m
//  SimpleTableViewApp
//
//  Created by Divya Munni on 2/25/16.
//  Copyright © 2016 app. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
